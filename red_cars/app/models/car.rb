class Car < ActiveRecord::Base
  attr_accessible :color, :brand
  belongs_to :user

  scope :with_color, -> (color) { where(color: color) }

  # Returns the number of cars for each colors.
  #
  # @return [Hash] example : {"green"=>3, "red"=>1} 
  def self.count_by_colors
    group(:color).count
  end
  
  # Returns all brands that has at least a car for specified color.
  #
  # @return [Array] example : ["Kia", "Renault"] 
  def self.brands_by_color(color)
    with_color(color).pluck(:brand).uniq
  end
end
