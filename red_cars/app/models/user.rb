class User < ActiveRecord::Base
  attr_accessible :first_name, :last_name
  has_many :cars

  scope :with_cars, -> (color) { joins(:cars).where(cars: {color: color}).uniq }
end