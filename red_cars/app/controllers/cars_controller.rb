class CarsController < ApplicationController
  def brands_by_color
    render json: Car.brands_by_color(params[:color])
  end
end
