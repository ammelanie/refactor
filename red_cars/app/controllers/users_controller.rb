class UsersController < ApplicationController
  def index
    @color = params[:color]
    @users = User.with_cars(@color)
  end

  def show
    @user = User.find params[:id]
  end
end
