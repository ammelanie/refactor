RedCars::Application.routes.draw do
  get 'cars/:color/brands', to: 'cars#brands_by_color'

  resources :users, only: [:show]
  get 'cars/:color/users', to: 'users#index'
end
