class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :first_name
      t.string :last_name
      t.references :organisation

      t.timestamps
    end
    add_index :users, :organisation_id
  end
end
