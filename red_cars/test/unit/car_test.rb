require 'test_helper'

class CarTest < ActiveSupport::TestCase
  test 'should be valid with no data' do
    assert Car.new.valid?
  end
  
  test 'should have brand and color set with mass assignment' do
    car = Car.create! brand: 'Kia', color: 'red'
    assert_equal car.brand, 'Kia'
    assert_equal car.color, 'red'
  end
  
  test 'count_by_colors should retrieve how many times each car color appears' do
    car_colors = Car.count_by_colors
    assert_equal 4, car_colors.count
    assert_equal 3, car_colors['red']
    assert_equal 1, car_colors['blue']
    assert_equal 1, car_colors['white']
    assert_equal 1, car_colors['green']
  end
  
  test 'with_color should retrieve all cars with the given color' do
    red_cars = Car.with_color('red')
    assert_equal 3, red_cars.count
    assert red_cars.include?(cars(:red_renault))
    assert red_cars.include?(cars(:red_kia))
    assert red_cars.include?(cars(:red_kia2))
  end
end
