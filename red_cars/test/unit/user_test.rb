require 'test_helper'

class UserTest < ActiveSupport::TestCase
  test 'should be valid with no data' do
    assert User.new.valid?
  end
  
  test 'should have first name and last name set with mass assignment' do
    user = User.create! first_name: 'John', last_name: 'Doe'
    assert_equal user.first_name, 'John'
    assert_equal user.last_name, 'Doe'
  end  

  test 'users_with_red_cars should retrieve all users getting a red car' do
    users_with_red_cars = User.with_cars('red')
    assert_equal 2, users_with_red_cars.to_a.count
    assert users_with_red_cars.include? users(:john_doe)
    assert users_with_red_cars.include? users(:mohamed_ali)
    assert !users_with_red_cars.include?(users(:bob_marley))
    assert !users_with_red_cars.include?(users(:rick_morty))
  end
  
  test 'users_with_red_cars should retrieve each user with a red car' do
    users_with_red_cars = User.with_cars('red')
    assert_equal 2, users_with_red_cars.to_a.count
    assert_equal 1, users_with_red_cars.to_a.count(users(:john_doe))
    assert_equal 1, users_with_red_cars.to_a.count(users(:mohamed_ali))
  end
end
