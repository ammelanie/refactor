require 'test_helper'

class CarsControllerTest < ActionController::TestCase
  test "should get red cars' unique brands" do
    get :brands_by_color, color: 'red'
    assert_response :success
    assert_equal ["Kia", "Renault"].to_json, @response.body
  end
end
