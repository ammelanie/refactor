require 'test_helper'

class UsersControllerTest < ActionController::TestCase
  test 'should get show' do
    john_doe = users(:john_doe)
    get :show, { id: john_doe.id } 
    assert_response :success
    
    user = assigns(:user)
    assert_not_nil user
    assert_equal john_doe.first_name, user.first_name
    assert_equal john_doe.last_name, user.last_name
  end

  test 'should get index with all users having a car of specified color' do
    get :index, { color: 'red' } 
    assert_response :success
    
    users = assigns(:users)
    assert_not_nil users
    assert_equal 2, users.to_a.count
    assert users.include?(users(:john_doe))
    assert users.include?(users(:mohamed_ali))
  end
end
