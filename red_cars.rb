class Car < ActiveRecord::Base
  # attr_accessible n'existe plus en rails 5 (maj potentiellement importante maintenabilité/fiabilité/sécurité) 
  # et a été sorti dans un gem à part en rails 4
  # à la place : utilisation des strong parameters [params.require().permit()] qui 
  # permettent notamment de faire varier les autorisations en fonction du context
  attr_accessible :color, :brand
  belongs_to :user

  def self.count_by_colors
    # Requête SQL écrite au lieu d'utiliser ActiveRecord peut-être valide si application
    # nécessitant de grosses performances // sinon gain en maintenabilité / lisibilité
    # tests effectués sous sqlite : 0.000846193 vs 0.019860221 (ar rails 3) vs 0.006452681 (rails 4) vs 0.019101492 (rails 5)
    rows = connection.execute(<<-SQL)
      SELECT color AS COLOR, COUNT(*) AS count
      FROM cars
      GROUP BY color
    SQL
    # Problème de parsing du résultat 'color' à la place de 'COLOR'
    # problème dans l'utilisation de la méthode each_with_object => inversement de l'accumulateur et de la row
    rows.each_with_object({}) { |acc, row| acc[row['color']] = row['count'] }
  end
end

class Organisation < ActiveRecord::Base
  has_many :users

end

class User < ActiveRecord::Base
  # même chose que pour le attr_accessible de Car
  attr_accessible :first_name, :last_name
  has_many :cars
  # pas de belongs_to :organisation => non nécessaire de récupérer l'organisation d'un utilisateur dans l'application

  # Restrictif à la couleur 'red'
  # 'users' dans le nom est en doublon car déjà dans la classe User
  # résultat qui renvoi plusieurs fois le même utilisateur s'il a plusieurs voitures rouges
  # requête de récupération des ids potentiellement en trop
  # retourne un Array plutôt qu'un subset d'utilisateur
  def self.users_with_red_cars
    car_ids = Car.where(color: 'red').pluck(:id)
    User.joins(:cars).where(cars: { id: car_ids }).to_a
  end
end

class UserController < ActionController::Base

  # Injection SQL possible  /users/5) OR (last_name="Marley" par exemple
  # render @user va rendre la partial associé et non pas la vue show
  def show
    @user = User.where("id = #{params[:id]}").to_a.first
    render @user
  end

  # Méthode qui semble plus être du debug / peu utile car uniquement des puts
  def print_users_with_red_cars
    User.users_with_red_cars.each do |user|
      puts user.first_name + ' ' + user.last_name + ' has a red car'
    end
  end

  # Méthode qui retourne des marques de voitures, sans trop de rapport avec les utilisateurs
  # Restrictif à la couleur 'red'
  # Pas très performant car des requêtes dans une boucle
  # Requêtes pas toutes pertinentes car on peut directement récupérer l'information en passant uniquement par Car
  def red_brands
    results = []
    User.users_with_red_cars.each do |user|
      user.cars.each { |car| results << car.brand if car.color == 'red' && !results.include?(car.brand) }
    end
    render json: results.to_json
  end
end